const { NxAppWebpackPlugin } = require('@nx/webpack/app-plugin');
const { join } = require('path');

module.exports = {
  output: {
    path: join(__dirname, 'swagger/configuration'),
  },
  plugins: [
    new NxAppWebpackPlugin({
      target: 'node',
      transformers: ['@nestjs/swagger/plugin'],
      fileReplacements: [
        {
          replace: './src/environments/environment.ts',
          with: './src/environments/environment.swagger.ts',
        },
      ],
      compiler: 'tsc',
      main: './src/swagger.ts',
      tsConfig: './tsconfig.app.json',
      assets: ['./src/assets'],
      optimization: false,
      outputHashing: 'none',
      generatePackageJson: true,
    }),
  ],
};
