/* eslint-disable */
export default {
  displayName: 'configuration',
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: 'junit/apps/settings',
        suiteName: 'configuration',
        uniqueOutputName: true,
        classNameTemplate: '{classname}',
        titleTemplate: '{title}',
        usePathForSuiteName: 'true'
      }
    ]
  ],
  preset: './jest.preset.js',
  testEnvironment: 'node',
  transform: {
    '^.+\\.[tj]s$': [ 'ts-jest', { tsconfig: '<rootDir>/tsconfig.spec.json' } ]
  },
  moduleFileExtensions: [ 'ts', 'js', 'html' ],
  coverageDirectory: './coverage/configuration',
  testMatch: [
    '<rootDir>/src/**/__tests__/**/*.[jt]s?(x)',
    '<rootDir>/src/**/*(*.)@(spec|test).[jt]s?(x)'
  ]
};
