import {
  Logger,
  Module
} from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { ConfigurationHealthIndicator } from './configuration-health-indicator.service';
import { HealthController } from './health.controller';

@Module({
  imports: [ TerminusModule ],
  providers: [ ConfigurationHealthIndicator ],
  controllers: [ HealthController ]
})
export class HealthModule {
}
