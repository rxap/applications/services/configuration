RxAP configuration service
==========================

# Getting started

## Introduction

The RxAP Configuration Service is designed to manage and serve configurations for Angular applications. It enables the
definition of general configurations applicable across all applications or specific configurations tailored to
individual applications and their versions.

### Prerequisites

- Docker and Docker Compose installed on your system, with Docker Compose version 3 or higher.

## Installation

Use the following Docker Compose configuration to integrate the RxAP Configuration Service into your setup. The service
comes with Traefik labels included; you only need to define necessary entrypoints for HTTP/HTTPS as per your
requirement.

```yaml
version: "3.7"
services:
  rxap-service-configuration:
    image: registry.gitlab.com/rxap/applications/services/configuration:${RXAP_SERVICE_CONFIGURATION:-development}
    volumes:
      - ./shared/service/configuration:/app/assets
```

## Configuration

The service scans the `/app/assets` directory for configuration files. At a minimum, a `latest` folder containing
a `config.json` file is required. For version-specific configurations, create folders named after the version (
e.g., `5.4.3`, `6.0.1`) with their respective configuration files.

### Version Determination Logic

The RxAP Configuration Service employs a sophisticated method to determine which version of the configuration to serve
to an application. This logic is especially important for ensuring that applications receive the most compatible
configuration version available.

Here's how the version determination logic works:

- The service maintains a sorted list of all available semantic version numbers for configurations.
- When an application requests its configuration, the service compares the application's version against this list.
- The service selects the highest version of the configuration that is less than or equal to the application's version.
- If no such version exists (i.e., the application's version is lower than any available configuration version), the
  service defaults to using the configuration from the `latest` folder.

This approach ensures that applications are always provided with the most appropriate configuration for their version,
falling back to a general configuration if no specific match is found.

### Application-Specific Configurations

To define configurations for a specific application within a version, create a folder named after the application inside
the version folder. Keys in application-specific configurations override those in the general configuration.

### Configuration Folder Structure Example

```
- latest
  - portal
    - config.sentry.json
  - config.logo.json
  - config.json
- 6.3.0
  - admin
    - config.api.json
  - config.i18n.json
- 7.4.2
  - config.api.json
```

### Configuration Merging Examples

The service allows for both general configurations and application-specific configurations. When both are present,
configurations are merged, with application-specific keys overriding general configuration keys. Here's an example to
illustrate this:

- **General Configuration (`config.json`):**
  ```json
  {
    "apiUrl": "https://api.example.com",
    "featureFlags": {
      "newFeature": false
    }
  }
  ```

- **Application-Specific Configuration (`[app-name]/config.json`):**
  ```json
  {
    "apiUrl": "https://api.special.example.com"
  }
  ```

- **Merged Configuration for `[app-name]`:**
  ```json
  {
    "apiUrl": "https://api.special.example.com",
    "featureFlags": {
      "newFeature": false
    }
  }
  ```

In this example, the `apiUrl` is overridden by the application-specific configuration, while the `newFeature` flag
remains as defined in the general configuration.

### Understanding `propertyPath` in Configuration Filenames

The `propertyPath` in configuration filenames allows for defining nested configuration properties directly through the
file structure. For example, a file named `config.featureFlags.newFeature.json` would directly modify the `newFeature`
key inside the `featureFlags` object of the merged configuration.

- **File Name:** `config.featureFlags.newFeature.json`
- **Contents:**
  ```json
  {
    "enabled": true
  }
  ```

- **Merged Configuration:**
  ```json
  {
    "apiUrl": "https://api.special.example.com",
    "featureFlags": {
      "newFeature": {
        "enabled": true
      }
    }
  }
  ```

This mechanism provides a flexible and intuitive way to organize and override configurations at any level of the
configuration object's hierarchy.

## API Endpoints Overview

The RxAP Configuration Service provides two primary API endpoints for accessing configurations:

1. **Latest Configuration for an Application:**

    - Endpoint: `/latest/{application}`
    - Method: `GET`
    - Description: Fetches the most recent configuration for the specified application.

2. **Specific Version Configuration for an Application:**

    - Endpoint: `/[version]/{application}`
    - Method: `GET`
    - Description: Fetches the configuration for a specific version of the specified application.

These endpoints ensure that applications can retrieve their necessary configurations either based on the latest
available or a specific version, depending on their needs.

## Accessing the Service

- The default global API prefix is `api/configuration`, customizable via the `GLOBAL_API_PREFIX` environment variable.
- The service is accessible without authentication.
- Request throttling is enabled; adjust the `THROTTLER_LIMIT` and `THROTTLER_TTL` environment variables as needed.

## API Endpoints

Refer to the provided OpenAPI specification for details on available endpoints, including fetching the latest
configuration for an application or a configuration specific to a version and application.

## Troubleshooting

No known issues are present. Logging level is set to `warn` by default, adjustable via the `LOG_LEVEL` environment
variable (`error`, `warn`, `log`, `debug`, `verbose`).
